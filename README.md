# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm Project - Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：[https://midterm-project---chatroom.web.app/private.html](https://midterm-project---chatroom.web.app/private.html)

## Website Detail Description

* Sign in page!
    * Sign in with email
    * Sign in with gmail
    * Sign up with email
![](https://i.imgur.com/mnJcS9F.png)
* Public room
    * See the recent message everybody update.
    * Post your message and everyone can see.
![](https://i.imgur.com/JDZMQI5.png)
* Private room
    * Create a private room
    * Select the room you create
    * Invite your friends
    * Post your message to the people you invited
![](https://i.imgur.com/YeIVtTs.png)


# Components Description : 
* Sign up and Sign in
    * Signed up a new account, and you have to type a username
    * if you sign in with google, and if you haven't have a username, then you have to type a username too
    ![](https://i.imgur.com/GEYoN70.png)
    

    
* Private room:
    * You can type the room and create
![](https://i.imgur.com/4cyqrNy.png)
    * If successfully, alert us we are in which room.
![](https://i.imgur.com/Uay9ueE.png)
    * If failed, input again.
![](https://i.imgur.com/nR9Ejwh.png)
* You can Invite people. If successfully invited, you can chat with him/her.
    * Your message will on the right side, and your friends' will on the left side
    ![](https://i.imgur.com/RTesrM8.png)










# Other Functions Description : 
* Animation: 
    * the bubble will bloobloo in background
    ![](https://i.imgur.com/4ChxcyH.gif

* Dealing with html code
    * You can add '<', '>' into message
    ![](https://i.imgur.com/gY1sgQZ.png)

* If your friends send a message, you will get notification
    * the (5) in the notification block is your friends username.
    ![](https://i.imgur.com/l7v1nug.png)
