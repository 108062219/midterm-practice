function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()" and clean input field
        // console.log(txtEmail);
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(user){
            console.log(user);
            create_alert("success", "success!");
            location.href = "index.html";
        })
        .catch(function(error){
            var errorMessage = error.message;
            txtEmail.value = "";
            txtPassword.value = "";
            create_alert("error", errorMessage);
        });

        
        
    });

    btnGoogle.addEventListener('click', function() {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert()"
        var provider = new firebase.auth.GoogleAuthProvider();
        var Ref = firebase.database().ref('contact_list');
        var flag = false;
        firebase.auth().signInWithPopup(provider).then(function(result){

            var user = result.user;
            // console.log(user.email);
            Ref.once('value').then((snapshot) => {
                snapshot.forEach(function(childshot){
                    var childData = childshot.val();
                    console.log(childData.email);
                    if(childData.email == user.email){
                        flag = true;
                        console.log('got!');
                    }
                });
                
            }).then(function(){
                if(!flag){
                    var result = prompt("You haven't resgistered!, input one.");
                    if(!result)return;
                    user.updateProfile({
                        displayName: result
                    }).then(function(){
                        var data = {
                            email: user.email,
                            name: result
                        };
                        Ref.child(result).set(data);
                        create_alert("success", "success log in");
                        location.href = "index.html";
                    })
                }else{
                    create_alert("success", "success log in");
                    location.href = "index.html";
                }
                
            })
            
            
        }).catch(function(error){
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        })
    });

    btnSignUp.addEventListener('click', function() {
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        var result = prompt('input the username!');
        if(!result)return;
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
        .then((userCredential)=>{
            var user = userCredential.user;
            user.updateProfile({
                displayName: result
            }).then(function(){
                
                var Refname = firebase.database().ref('contact_list');
                var data = {
                    email: user.email,
                    name: result
                };
                Refname.child(result).set(data);
            }).then(function(){
                create_alert("success", "You have created an account!");
                txtEmail.value = "";
                txtPassword.value = "";
            })
        })
        .catch((error) =>{
            var errorMessage = error.message;
            create_alert("error", errorMessage);
        })
        txtEmail.value = "";
        txtPassword.value = "";

    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};