var cur_room = "";
var user_profile = '';
function init() {
    var user_email = '';
    
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            user_profile = user.displayName;
            menu.innerHTML = "<a class='dropdown-item'>" + user.email + "</a><a class='dropdown-item' id='logout-btn'>Logout</a>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
                window.location.href = "menu.html";
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='#''>anonymous</a><a class='dropdown-item' href='menu.html'>Login</a>";
        }
    });


    var create_btn = document.getElementById("create_btn");
    var create_room = document.getElementById("create_room");

    var invite_friends = [];

    // push the room to the left column list
    create_btn.addEventListener('click', function(){

        var Ref = firebase.database().ref('private');
        //check if the room have been created
        var flag = false;
        Ref.once('value').then(function(snapshot){
            snapshot.forEach(function(childshot){
                childData = childshot.val();
                if(childData.room == create_room.value){
                    flag = true;
                }
            })
            if(flag) {
                create_alert("error", "the room is already existed");
            } else {
                create_alert("success", "You are now in" + cur_room);
                invite_friends = [];
                invite_friends[0] = user_profile;
                
        
                var data = {
                    member: invite_friends,
                    room: create_room.value
                };
                Ref.child(create_room.value).set(data);
                cur_room = create_room.value;
                create_alert("success", "You are now in " + cur_room);
            }
            create_room.value = "";
        })
        
    })

    var btn_invite = document.getElementById("btn_invite");


    // invites friends who is registered.
    btn_invite.addEventListener('click', function(){
        var Ref = firebase.database().ref('private');
        var flag = false;
        var friends = prompt('input the friends u wanna input. The format can be email or username');
        if(cur_room == "" || friends == "" || friends == null){
            alert('try again!');
        }else{
            //test if he is registered
            var Refname = firebase.database().ref('contact_list');
            
            Refname.once('value').then((snapshot) => {
                snapshot.forEach(function(childshot){
                    var childData = childshot.val();
                    if(childData.name == friends){
                        flag = true;
                    }else if(childData.email == friends){
                        flag = true;
                        friends = childData.name;
                    }
                })
            })
            .then(function(){
                if(!flag) {
                    create_alert('error', 'the member has not registered');
                    return;
                }
                Ref.once('value').then((snapshot) => {
                    invite_friends = snapshot.val()[cur_room].member;
                }).then(()=>{
                    flag = false;
                    for(var i=0; i<invite_friends.length; i++){
                        if(invite_friends[i] == friends){
                            create_alert('error', "he/she is already invited");
                            flag = true;
                            break;
                        }
                    }
                }).then(()=>{
                    if(!flag){
                        invite_friends[invite_friends.length] = friends;
                        Ref.child(cur_room).update({member: invite_friends});
                        create_alert('success', "you have invited " + friends);
                    }
                })
            })
        }
    });


    var btn_send = document.getElementById('btn_send');
    var post_txt = document.getElementById('post_txt');

    btn_send.addEventListener('click', function(){
        if( post_txt.value!= ""){
            var Ref = firebase.database().ref('private');
            
                
            var data = {
                data: post_txt.value,
                user: user_profile
            };
            Ref.child(cur_room).child('data').push(data);
            post_txt.value = "";
            // var str_imp = post_txt.value.replaceAll('<', '&lt');
            // var str_imp = str_imp.replaceAll('>', '&gt');
            // total_hist_post[total_hist_post.length] = str_before_history_me + str_imp + '</p>' + str_after_history_me;
            // post_txt.value = "";
            // document.getElementById('history').innerHTML = total_hist_post.join('');
        }
    })


    //get the room on the left column
    var postsRef = firebase.database().ref('private');
    var total_room = [];
    var total_history_chat = [];
    var first_count = 0;
    var second_count = 0;
    var str_before_inbox_room =
    "<div class='chat_list active_chat'>\
            <div class='chat_people'>\
            <div class='chat_img'> <img src='https://ptetutorials.com/images/user-profile.png' alt='sunil'> </div>\
            <div class='chat_ib'></div>\
                <button type='button' class='btn btn-success'";
    var str_after_inbox_room =
                '</button>\
        </div>\
    </div>\n'



    document.getElementById('private_room_name').innerHTML = "";
    postsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot){
                var childData = childshot.val();
                first_count += 1;
                var flag = false;
                cur_room = childData.room;
                //if user is in the contact list, print the room.
                for(var i=0; i<childData.member.length; i++){
                    if(childData.member[i] == user_profile){
                        flag = true;
                        break;
                    }
                }
                if(flag){
                    total_room[total_room.length] = str_before_inbox_room + "value='" + childData.room + "'" + "id='" + childData.room + "' onclick='room_change(this.id)'>" + childData.room + str_after_inbox_room;
                }

            })



            document.getElementById('private_room_name').innerHTML = total_room.join('');
            postsRef.on('child_added', function(data) {
                var childData = data.val();
                second_count += 1;
                if(second_count > first_count){
                    var flag = false;
                    for(var i=0; i<childData.member.length; i++){
                        if(childData.member[i] == user_profile){
                            flag = true;
                            break;
                        }
                    }
                    if(flag){
                        total_room[total_room.length] = str_before_inbox_room + "value='" + childData.room + "'" + "id='" + childData.room + "' onclick='room_change(this.id)'>" + childData.room + str_after_inbox_room;
                        document.getElementById('private_room_name').innerHTML = total_room.join('');
                    }
                }
            })
        })



}
var total_hist_post = []
 // Counter for checking history post update complete
 var first_hist_count = 0;
 // Counter for checking when to update new post
 var second_hist_count = 0;

var str_before_history =
    '<div class="incoming_msg">\
        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>\
        <div class="received_msg">\
            <div class="received_withd_msg">\
            <p>';
var str_after_history =
'       </span></div>\
    </div>\
</div>\n'

var str_before_history_me = 
'<div class="outgoing_msg">\
    <div class="sent_msg">\
        <p>';
var str_after_history_me = 
    '</div>\
</div>'
function room_change(room){
    document.getElementById('history').innerHTML = "";
    total_hist_post = [];
    cur_room = room;
    var first_count_mes = 0;
    var second_count_mes = 0;
    console.log("current room = " + cur_room);
    var postsRef = firebase.database().ref('private');
    postsRef.child(cur_room).child('data').once('value').then(function(snapshot){
        
        snapshot.forEach(function(childshot){
            var history = childshot.val();
            first_count_mes++;
            var str_imp = history.data.replaceAll('<', '&lt');
            str_imp = str_imp.replaceAll('>', '&gt');
            if(history.user != user_profile){
                total_hist_post[total_hist_post.length] = str_before_history + str_imp + '</p>' + '<span class="time_date"> user name: ' + history.user + str_after_history;
                document.getElementById('history').innerHTML = total_hist_post.join('');
                
            }
            else {
                total_hist_post[total_hist_post.length] = str_before_history_me + str_imp + '</p>' + str_after_history_me;
                document.getElementById('history').innerHTML = total_hist_post.join('');
            }
            

        });

        postsRef.child(cur_room).child('data').on('child_added', function(data){
            second_count_mes++;
            if(second_count_mes > first_count_mes){
                var childData = data.val();
                
                var str_imp = childData.data.replaceAll('<', '&lt');
                var str_imp = str_imp.replaceAll('>', '&gt');
                // console.log(str_imp);
                if(childData.user != user_profile){
                    total_hist_post[total_hist_post.length] = str_before_history + str_imp + '</p>' + '<span class="time_date"> user name: ' + childData.user + str_after_history;
                    document.getElementById('history').innerHTML = total_hist_post.join('');
                    notifyMe();
                }
                else {
                    total_hist_post[total_hist_post.length] = str_before_history_me + str_imp + '</p>' + str_after_history_me;
                    document.getElementById('history').innerHTML = total_hist_post.join('');
                }
            }
        })
        // console.log(snapshot);
    });
}
window.onload = function() {
    init();
};

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function notifyMe() {
    // 首先讓我們確定瀏覽器支援 Notification
    if (!("Notification" in window)) {
      alert("no supported!");
    }
  // check if permission is denied
    else if (Notification.permission === "granted") {
      var notification = new Notification("(" + user_profile + ") someone send you a message");
    }
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          var notification = new Notification("you have admitted!");
        }
      });
    }

  }